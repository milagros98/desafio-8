import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { GaleriaComponent } from './galeria/galeria.component';



@NgModule({
  declarations: [
    InicioComponent,
    GaleriaComponent
  ],
  imports: [
    CommonModule
  ]
})
export class RoutesModule { }
